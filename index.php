<!doctype html>
<html lang="pt-br">
  <head>
    <!-- meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <title>Projeto</title>
  </head>
  <body>
      <div class="container">
          <div class="well">
              <div class="jumbotron">
                <h1>Olá Usuário! </h1>
                <h6>Entre com seus dados para acessar o sistema</h6>
                
                <div class="center-block">
                    <form class="form-horizontal" method="post" action="bat/validate-login.php">
                      <div class="form-group">
                        <label for="loginUser" class="col-sm-2 control-label">E-mail</label>
                        <div class="col-sm-10">
                          <input type="email" id="loginUser" name="loginUser" class="form-control" placeholder="E-mail">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label for="loginPass" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                          <input type="password" id="loginPass" name="loginPass" class="form-control" placeholder="Senha">
                        </div>
                      </div>
                
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary btn-lg">Acessar</button>
                        </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- JavaScript -->
    <script src="js/jquery-1.11.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>