<?php 

class Conexao {
    
    const HOST = "localhost";
    const DATABASE = "db_teste";
    const USER = "root";
    const PASS = "teste123";
    
    protected $conexao;
    
    function __construct() {
        
        $this->conexao = new PDO("mysql:host=".self::HOST.";dbname=". self::DATABASE, self::USER, self::PASS);
     
        
        return $this->conexao;
        
    }

    
}