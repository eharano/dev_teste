<?php 

require_once dirname(__FILE__) . '/Conexao.php';

class Usuario {
    
    protected $conexao;
    
    public function __construct(){
        
        $this->conexao = new Conexao();
        
    }
   
    public function adicionar($nome,$email,$senha){

        $sql = "INSERT INTO USUARIO (NOME, EMAIL, PASS) VALUES (?,?,?)";
        $stmt = $this->conexao->prepare($sql);
        $stmt->bindParam(1, $nome);
        $stmt->bindParam(2, $email);
        $stmt->bindParam(3, $senha);
        
        return $stmt->execute();

    }
    
    public function validaLogin($email,$senha){
        
        $sql = "SELECT CODIGO FROM USUARIO WHERE EMAIL = ? AND SENHA = ?";
        $stmt = $this->conexao->prepare($sql);
        $stmt->bindParam(1, $email, PDO::PARAM_STR);
        $stmt->bindParam(2, $senha, PDO::PARAM_STR);
        
        return $stmt->execute();
        
    }
    
    public function listar(){
        
        $sql = "SELECT CODIGO
                       , NOME
                       , EMAIL
                  FROM USUARIO
                 ORDER BY NOME ASC  
            ";
        
        return $this->conexao->query($sql);
        
    }
    
}

