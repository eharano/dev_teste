<?php 

require_once dirname(__FILE__) . '/Conexao.php';

class Calculo {

    protected $conexao;
    
    public function __construct(){
        
        $this->conexao = new Conexao();
        
    }
    
    public function soma($valor1, $valor2){
        $calculo =  $valor1 ." + ". $valor2;
        $this->geraLog($calculo);
        
        return $valor1 + $valor2;
    }
    
    public function subtracao($valor1, $valor2){
        $calculo =  $valor1 ." - ". $valor2;
        $this->geraLog($calculo);
        
        return $valor1 - $valor2;
    }
    
    public function divisao($valor1, $valor2){
        $calculo =  $valor1 ." / ". $valor2;
        $this->geraLog($calculo);
        
        return $valor1 / $valor2;
    }
    
    public function multiplicacao($valor1, $valor2){
        $calculo =  $valor1 ." * ". $valor2;
        $this->geraLog($calculo);
        
        return $valor1 * $valor2;
    }
    
    public function porcentagem($valor1, $valor2){
        $calculo =  $valor1 ." % ". $valor2;
        $this->geraLog($calculo);
        
        return $valor1 % $valor2;
    }
    
    public function raizQuadrada($valor){
        
        for ($i=0; $i*$i<=$valor; $i++){
            $result = $i*$i;
            $i++;
        }
        
        $y = $i-1;
        $v1 = ( $i * $i) - $valor;
        $v2 = $valor - ( $y * $y );
        
        if ( $v1 < $v2 ){
            $result = $i * $i;
        }
        else if ( $v1 > $v2 ){
            $result = $y * $y;
            --$i;
        }
        
        $this->geraLog('&radic; '. $valor);
        
        return ($valor+$result)/(2*$i);
        
    }
    
    public function geraLog($calculo){

        $sql = "INSERT INTO CALCULADORA_HISTORICO (CALCULO, USUARIO) VALUES (?,?)";
        $stmt = $this->conexao->prepare($sql);
        $stmt->bindParam(1, $calculo, PDO::PARAM_STR);
        $stmt->bindParam(2, $_SESSION['usuario'], PDO::PARAM_INT);
        $stmt->execute();

    }
    
    
    public function listar(){
        
        $sql = "SELECT CALCULO FROM CALCULADORA_HISTORICO WHERE USUARIO = ? ORDER BY CODIGO ASC";
        
        $stmt = $this->conexao->prepare($sql);
        $stmt->bindParam(1, $_SESSION['usuario'], PDO::PARAM_INT);
        return $stmt->execute();
        
    }
    
}

