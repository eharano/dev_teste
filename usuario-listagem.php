<?php include_once 'io.php'; ?>
<!doctype html>
<html lang="pt-br">
<head>
<!-- meta tags -->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta charset="utf-8">

<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.min.css" />
<title>Projeto</title>

</head>
<body>
<?php include 'navbar.php'; ?>

<div class="container">
      <div class="well">
          <div class="jumbotron">
            <span> Início &raquo; Usuário </span>
            <h1>Listagem de Usuário</h1>
            

            <table class="table">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nome</th>
                  <th scope="col">E-mail</th>
                </tr>
              </thead>
              
              <tbody>
              <?php 
              $Usuario = new Usuario();
              $lista = $Usuario->listar();
              foreach ( $lista as $key => $dados){
              ?>
                <tr>
                  <th scope="row"><?=($key+1)?></th>
                  <td><?=$dados['NOME']?></td>
                  <td><?=$dados['EMAIL']?></td>
                </tr>
              <?php } ?>
                
              </tbody>
            </table>
            
    	</div>
	</div>
</div>

<!-- JavaScript -->
<script src="js/jquery-1.11.3.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>