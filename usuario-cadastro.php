<?php include_once 'io.php'; ?>
<!doctype html>
<html lang="pt-br">
<head>
<!-- meta tags -->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta charset="utf-8">

<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.min.css" />
<title>Projeto</title>

</head>
<body>
<?php include 'navbar.php'; ?>

<div class="container">
      <div class="well">
          <div class="jumbotron">
            <span> Início &raquo; Usuário </span>
            <h1>Cadastro de Usuário</h1>
            
              <form class="rd-mailform form-horizontal" method="post" action="bat/adicionar-usuario.php">
              
                <div class="form-group">
                  <label class="control-label col-sm-2" for="nome">Nome:</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="nome" placeholder="Nome" name="nome" data-constraints="@NotEmpty @LettersOnly">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-2" for="email">E-mail:</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" placeholder="E-mail" name="email" data-constraints="@NotEmpty @Email">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-2" for="pwd">Senha:</label>
                  <div class="col-sm-10">          
                    <input type="password" class="form-control" id="pwd" placeholder="Senha" name="pwd" data-constraints="@NotEmpty">
                  </div>
                </div>
   
                <div class="form-group">        
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Salvar</button>
                  </div>
                </div>
                
              </form>
            
    	</div>
	</div>
</div>

<!-- JavaScript -->
<script src="js/jquery-1.11.3.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>