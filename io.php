<?php 

session_start();

if ( !isset($_SESSION['acesso']) ){
    unset($_SESSION);
    header("Location: index.php");
    exit;
}

function __autoload($className){
    
    $arquivo = dirname(__FILE__) . "/lib/" . $className .".php";
    
    if ( file_exists($arquivo) ){
        
        require_once $arquivo;
        
    }
    
}