
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="navbar-header bg-white">
    	<a class="navbar-brand ">Projeto Teste</a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active"><a class="nav-link" href="home.php">Home</a></li>           
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Usuário
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="usuario-cadastro.php">Cadastro</a>
                  <a class="dropdown-item" href="usuario-listagem.php">Listagem</a>
                </div>
              </li>
              
            <li class="nav-item"><a class="nav-link" href="calculadora.php">Calculadora</a></li>
            <!-- <li class="nav-item"><a class="nav-link" href="relatorio.php">Relatório</a></li>  -->
        </ul>
    </div>
</nav>
