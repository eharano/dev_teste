<?php 

ob_start();
require_once  '../lib/Calculo.php';

$Calculo = new Calculo();

$str = $_POST['calcular'];
$raizQuadrada = $_POST['raizQuadrada'] ? true : false;

if ( strrpos($str, '+') ){
    list( $valor1, $valor2 ) = explode('+', $str);
    $total = $Calculo->soma( $valor1, $valor2 );
}
else if ( strrpos($str, '-') ){
    list( $valor1, $valor2 ) = explode('-', $str);
    $total = $Calculo->subtracao( $valor1, $valor2 );
}
else if ( strrpos($str, '*') ){
    list( $valor1, $valor2 ) = explode('*', $str);
    $total = $Calculo->multiplicacao( $valor1, $valor2 );
}
else if ( strrpos($str, '/') ){
    list( $valor1, $valor2 ) = explode('/', $str);
    $total = $Calculo->divisao( $valor1, $valor2 );
}
else if ( strrpos($str, '%') ){
    list( $valor1, $valor2 ) = explode('/', $str);
    $total = $Calculo->porcentagem( $valor1, $valor2 );
}
else if ( $raizQuadrada ){
    $total = $Calculo->raizQuadrada( $str );
}

echo $total;


ob_end_flush();
