<?php include_once 'io.php'; ?>
<!doctype html>
<html lang="pt-br">
<head>
<!-- meta tags -->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta charset="utf-8">

<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.min.css" />

<title>Projeto</title>

</head>
<body>
<?php include 'navbar.php'; ?>

<div class="container">
      <div class="well">
          <div class="jumbotron">
            <h1>Início </h1>
        	
        	<h5>Olá Usuário, seja bem vindo ao Projeto Teste.</h5>
    	</div>
	</div>
</div>

<!-- JavaScript -->
<script src="js/jquery-1.11.3.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>