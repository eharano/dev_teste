<?php include_once 'io.php'; ?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <title>Projeto</title>
    <style type="text/css">
    .calculadora {
        width: 240px;
        border: 1px solid #a0a0a0;
        border-radius: 5px;
        padding: 8px;
        background-color: #a0a0a0;
        margin: 50px 0 0 100px;
    }
    
    .calculadora button {
        margin: 1px 1px 2px 1px;
        padding: 3px 5px;
        font-size: 2em;
        width: 50px;
    }
    
    .calculadora button:hover {
        background-color: gray;
        color: #c0c0c0;
    }
    
    .display {
        width: block;
        height: 50px;
        background-color: #c0c0c0;
        border: 1px solid #ccc;
        margin-bottom: 3px;
        font-size: 1.5em;
    } 
    
    .display:focus, .display[attr='readonly'] {
        background-color: #c0c0c0;
    }
    
    .btn-ce {
        font-size: 1.2em !important;
        padding: 12px 3px !important;
    }
    
    .btn-zero {
        width: 107px !important;
    }
    
    .btn-result {
        height: 114px;
        float: right;
    }

    </style>
    <script type="text/javascript">
	function add(valor, calculo){
		var str = $('#display').val();		
		if ( calculo === true ){
			if ( str.length > 0 ){
    			if ( str.indexOf("-") >= 0 || str.indexOf("+") >= 0 || str.indexOf("/") >= 0 || str.indexOf("*") >= 0 || str.indexOf("%") >= 0 ){
    				calcular();
    				return add(valor, false);
    			}
			}
			else {
				alert('O primero dígito deve ser do tipo número.');
				return false;
			}
		}
		else if ( calculo !== false ){
			var n = str.length;
			var u = str.substr( n-1 );
			if ( u == '/' && valor == '0' ){
				alert('Não é possível fazer divisão por zero.');
				return false;
			}
			else if ( str.indexOf('.') >= 0 && u == '.' && valor == '.' ){
				alert('Não é possível adicionar mais de um ponto (.)');
				return false;
			}
		}
		$('#display').val(str + valor);
	}

	function limpar(){
		$('#display').val('');
	}
	
	function apagar(){
		var str = $('#display').val();
		var n = str.length;
		$('#display').val( str.substr(0, n-1) );
	}

	function calcular(raizQuadrada){
		$.ajax({
            type: "POST",
            url: './bat/calculo.php',
            data: {
				"calcular" : $('#display').val(),
				"raizQuadrada" : raizQuadrada
            },
            dataType: "text",
            success: function(data){
            	$('#display').val( data );
            }
		});
	}
    </script>
  </head>
  <body>
  	<?php include 'navbar.php'; ?>
  	
    <div class="container">
          <div class="well">
              <div class="jumbotron">
                <span> Início &raquo; Calculadora </span>
                <h1>Calculadora </h1>
                
                <div class="calculadora">
                	<input id="display" readonly="readonly" name="display" class="form-control display">
                	   
                	<div class="botoes">
                    	<!-- <button onclick="limpar()" class="btn btn-default btn-ce">CE</button> -->
                    	<button onclick="apagar()" class="btn btn-default">&larr;</button>
                    	<button onclick="add('%', true)" class="btn btn-default">%</button>
                    	<button onclick="calcular(true)" class="btn btn-default">&radic;</button>
                    	<button onclick="add('/', true)" class="btn btn-default">&divide;</button>
                		
            			<button onclick="add(7)" class="btn btn-default">7</button>
                    	<button onclick="add(8)" class="btn btn-default">8</button>
                    	<button onclick="add(9)" class="btn btn-default">9</button>
                    	<button onclick="add('*', true)" class="btn btn-default">&times;</button>
                    	
                    	<button onclick="add(4)" class="btn btn-default">4</button>
                    	<button onclick="add(5)" class="btn btn-default">5</button>
                    	<button onclick="add(6)" class="btn btn-default">6</button>
                    	<button onclick="add('-', true)" class="btn btn-default">-</button>
                    	
                    	<button onclick="add(1)" class="btn btn-default">1</button>
                    	<button onclick="add(2)" class="btn btn-default">2</button>
                    	<button onclick="add(3)" class="btn btn-default">3</button>
                    	<button onclick="add('+', true)" class="btn btn-default">+</button>
                    	
                    	<button onclick="add(0)" class="btn btn-default btn-zero">0</button>
                    	<button onclick="add('.')" class="btn btn-default">.</button>
                    	<button onclick="calcular()" class="btn btn-default">=</button>
                    	<!-- <button onclick="add(',')" class="btn btn-default">,</button> -->
                	</div>
            	</div>
            	
        	</div>
    	</div>
	</div>

    <!-- JavaScript -->
    <script src="js/jquery-1.11.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>